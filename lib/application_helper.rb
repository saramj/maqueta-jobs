module ApplicationHelper

  def get_index_files
    html_files = sitemap.resources.find_all{|p| p.ext == ".html" }
    html_files = html_files.map do |file|
      {
        :group => file.data.page_group,
        :destination_path => file.destination_path,
        :name => file.metadata[:page]["title"] ? file.metadata[:page]["title"] : file.destination_path,
        :description => file.metadata[:page]["description"]
      }
    end

    html_files.group_by do |file|
      if file[:group]
        file[:group]
      else
        "pages"
      end
    end
  end

  def index_sitemap
    index_sitemap = if request[:params][:refresh] || !get_index_files
      html_files = index_files
      dev_groups = Settings.dev_page_groups.inject({}) do |sum, group| 
        if html_files[group.to_s]
          sum[group] = html_files.delete group 
        end
        sum
      end
      html_files = html_files.sort{|key, content| key.to_s == "pages" ? 1 : 0}
      set_index_files({
        pages: html_files,
        dev: dev_groups
      })
    else
      get_index_files
    end
  end

  def page_list files
    output = (files || []).map do |page| 
      %~
      <li>
        <a href="#{page[:destination_path]}">
          #{page[:name]}
          #{ %~<p>#{page[:description]}</p>~ if page[:description] }
          #{ %~<small>#{page[:destination_path]}</small>~ if page[:destination_path] } 
        </a>        
      </li>
      ~
    end
    %~<ul class="app_index">#{output.join("")}</ul>~
  end
end